<footer class="footer inverse-wrapper">

	<?php get_template_part( 'inc/content-footer', 'widgets' ); ?>
	
	<div class="sub-footer">
		<div class="container inner">
			<p class="text-center">
				<?php 
					echo wpautop(
						wp_kses_post( 
							str_replace( 
								array( '*copy*', '*current_year*' ), 
								array( '&copy;', date( 'Y' ) ), 
								get_option( 'copyright', '*copy* *current_year* Lydia. By <a href="http://www.tommusrhodus.com">TommusRhodus</a>') 
							) 
						)
					); 
				?>
			</p>
		</div>
	</div>

</footer>
  
<div class="slide-portfolio-overlay"></div>

</main>

<a href="#0" class="slide-portfolio-item-content-close"><i class="budicon-cancel-1"></i></a>

<?php wp_footer(); ?>

<!-- CANDDi https://www.canddi.com/privacy -->
<script async type="text/javascript" src="//cdns.canddi.com/p/99de5f623dad9591e21ac9ed4e55c8ed.js"></script>
<noscript style='position: absolute; left: -10px;'><img src='https://i.canddi.com/i.gif?A=99de5f623dad9591e21ac9ed4e55c8ed(35 B)
https://i.canddi.com/i.gif?A=99de5f623dad9591e21ac9ed4e55c8ed
'/></noscript>
<!-- /END CANDDi -->

</body>
</html>