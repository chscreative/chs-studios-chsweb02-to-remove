<div class="col-xs-6 col-sm-4 col-lg-3 client-item">
	<?php 
	global $post; 
	$url = get_post_meta( $post->ID, '_ebor_client_url', true ); 
	$bg = get_post_meta( $post->ID, '_ebor_client_background', true ); 
	?>

	<div class="client-inner <?php if( $bg ) { echo 'has-bg';} ?>">

		<div class="client-logo">
			<?php if( $url ) { echo '<a href="'. esc_url( $url ) .'" target="_blank">';} ?>
			
			<?php	the_post_thumbnail( 'full' ); ?>
			
			<?php if( $url )  { echo '</a>'; } ?>
		</div>
		<div class="client-bg" style="background-image:url('<?php echo $bg; ?>')"></div>

	</div>
</div>