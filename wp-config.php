<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://codex.wordpress.org/Editing_wp-config.php

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

// define( 'DB_NAME', 'chs_food_photography_2019_local' ); // LOCAL

define( 'DB_NAME', "chsstudios_local" ); // LIVE


/** MySQL database username */

// define( 'DB_USER', 'chsroot' ); // LOCAL

define( 'DB_USER', "chsstudios" ); // LIVE


/** MySQL database password */

// define( 'DB_PASSWORD', '45e7Bb8oSwOD2pMH' ); // LOCAL

define( 'DB_PASSWORD', "1S5UXZ1A7FqBrfe5" ); // LIVE


/** MySQL hostname */

// define( 'DB_HOST', 'localhost' ); // LOCAL

define( 'DB_HOST', "localhost" ); // LIVE


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define('AUTH_KEY',         'Ytfd]U#h,;4t3A/9NzRS>K@fZYI*#fI92>$^-b7wKM23)yd5KF:%SqF&wP-c|#SK');

define('SECURE_AUTH_KEY',  '>{nFO-Q9wZBw7u$ pvH7?6E?:JxL+T;&`!gRLS?,mpytn`W[XBGF@x|aE98L9|.)');

define('LOGGED_IN_KEY',    '}`jRg8ot|rROC[liVKL9HI`-[ZmF/DqOowp#P+_2Y9;.)#U_gR4Rt&x C3z%6^H|');

define('NONCE_KEY',        '!R*k,TK;E4H@UzI`-@]voY,5+zZv84<Ag_rj{l_U3|!Zlnoq*JGG. 2[2B2w=`7!');

define('AUTH_SALT',        '[ShinJQ*Gj,V<)c_jE=.Hnr~:FCMFk0||Y.[ZeFHWsY.(_--^XkjG2}ANVn&$O3M');

define('SECURE_AUTH_SALT', 'BaF*SL 7%,JTGx+|{oTBFG~nV=%OiyOUQHJ&J%qBHZ0S0DQomhi[~MUn%8iB+X@{');

define('LOGGED_IN_SALT',   '>:{1HmaI$kf)uy|tV3BN7/7!6(}d=lPixM(h=qo|M=t2m0L~vE<C2KA5WNopI!85');

define('NONCE_SALT',       'YXR3WqT[va/,/]gQ:1F4S@kz>8^Q/)Gw*nS-ZHsr$HF=vD#Akm1z--+UsJ%V+Drh');


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the Codex.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

}


/** Sets up WordPress vars and included files. */

require_once( ABSPATH . 'wp-settings.php' );

